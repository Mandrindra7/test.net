import { configureStore } from '@reduxjs/toolkit'
import { combineReducers } from '@reduxjs/toolkit'

import userReducer from '../src/redux/user'
import ressourceReducer from '../src/redux/ressource'

const reducer = combineReducers({
    users: userReducer,
    ressources: ressourceReducer
  })
export const store = configureStore({
  reducer: reducer,
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch