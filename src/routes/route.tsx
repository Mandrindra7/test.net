import React from "react";
import { redirect } from "react-router";

import UserList from "../pages/UserList";
import UserDetail from "../pages/UserDetails";
import RessourceList from "../pages/RessourceList"
import RessourceDetail from "../pages/RessourceDetail"
import PageNotFound from "../pages/PageNotFound";



const routes = [
    {
        path: '/',
        redirect,
        element: <UserList />
    },
    {
        path : '/user-list',
        element: <UserList />
    },
    {
        path : '/user-detail/:id',
        element: <UserDetail />
    },
    {
        path : '/ressource-list',
        element: <RessourceList />
    },
    {
        path : '/ressource-detail/:id',
        element: <RessourceDetail />
    },
    {
        path : '*',
        element: <PageNotFound />
    },
   
]

export default routes;