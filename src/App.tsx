import { BrowserRouter as Router, Routes,Route ,Outlet} from "react-router-dom";

import routes from "./routes/route";
import './App.scss'
import NavBar from "./components/NavBar";

function App() {
  
  return (
    <div className="App">
      <Router>
        <NavBar />
        <Routes>
            {routes.map((props, index) => <Route {...props} key={index} />)}
        </Routes>  
        <Outlet/>

      </Router>
     
    </div>
  )
}

export default App
