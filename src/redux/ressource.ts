import { getAll, getRessource } from "../service/ressource";

import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { Ressource } from "../models/ressource";

const initialState = {
  lists: [],
};

export const getAllRessource = createAsyncThunk("ressource/getAll", async () => {
  const res = await getAll();
  return res && res.data;
});

export const getRessourceById = createAsyncThunk("ressource/getUserById", async (id: string) => {
  const res = await getRessource(id);
  return res && res.data;
});

export const filterTable = createAsyncThunk("ressource/filterTable", async (data: any) => {
  const res = await getAll();

  return res && res.data.filter((list: Ressource) => list[data.type as keyof Ressource]  === data.filter);
});

export const ressourceSlice = createSlice({
  name: "ressource",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getAllRessource.fulfilled, (state, action) => {
      state.lists = action.payload;
      return state;
    });
    builder.addCase(getRessourceById.fulfilled, (state, action) => {
      return action.payload;
    });
    builder.addCase(filterTable.fulfilled, (state, action) => {
      state.lists = action.payload;
      return state;
    });
  },
});

export const allRessource = (state: any) => state;

export default ressourceSlice.reducer;
