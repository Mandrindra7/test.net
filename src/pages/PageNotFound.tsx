import React from "react";
import { Link } from "react-router-dom";

import "../assets/pagenotfound.scss";

const PageNotFound = () => {
  return (
    <div className="page_not_found">
      <div className="container">
        <h1 className="title">
          404 <br />
          Not Found
        </h1>
        <p>
          <Link className="link" to="/user-list">
            Retour à la liste des utilisateurs
          </Link>
        </p>
      </div>
    </div>
  );
};

export default PageNotFound;
