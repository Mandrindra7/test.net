import React from 'react'

import { Ressource } from '../models/ressource'
import ('../assets/cardressource.scss')

const RessourceCard = (props : { ressource : Ressource}) => {
    const { ressource } = props
  return (
    <div className='card-ressource' style={{borderColor : ressource.color}}>
        <div className='card-item'>
            <p className='card-text'>{ressource.name}({ressource.pantone_value})</p>
            <p className='card-year' style={{color : ressource.color }}>{ressource.year}</p>
        </div>
    </div>
  )
}

export default RessourceCard