import React from 'react'
import { User } from '../models/user'

import '/src/assets/card.scss';

const Card = (props : {user: User}) => {
    const { user } = props
  return (
    <div className='card'>
        <img className='card-avatar' src={user.avatar}/> 
        <h1 className='card-title'>{user.first_name} {user.last_name}</h1>
        <p className='card-email'>{user.email}</p>
    </div>
  )
}

export default Card