import React, { useCallback, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router";

import RessourceCard from "../components/RessourceCard";
import { Ressource } from "../models/ressource";
import { allRessource, getAllRessource, filterTable } from "../redux/ressource";
import { AppDispatch } from "../store";
import "../assets/ressourcelist.scss";

const RessourceList = () => {
  const [name, setName] = useState()
  const [date, setDate] = useState()
  const { ressources } = useSelector(allRessource);
  const dispatch = useDispatch<AppDispatch>();
  const navigate = useNavigate();

  const getAll = useCallback(async () => {
    dispatch(getAllRessource());
  }, [dispatch]);

  useEffect(() => {
    getAll();
  }, [getAll]);

  const onChange = (e : any) => {
    console.log(e.target.value);
    e.target.value === "Name" && setName(e.target.value)
    e.target.value === "Date" && setDate(e.target.value)
  }

  const onFilter = (e: any) => {
    const filter = e.target.value
    let type;
    name &&( type = 'name')
    date &&( type = 'year')
    const data = { type, filter}
    type && dispatch(filterTable(data))
  }

  return (
    <div className="list-ressource">
      <div className="filter">
        <span >Filtre par</span>
        <select className="select-filter" onChange={onChange}>
          <option value={date}>Date</option>
          <option value={name}>Name</option>
        </select>
        {(name || date) && <input className="input" type="text" onChange={onFilter}/>}
      </div>
      <div className="list-item-ressource">
        {ressources?.lists?.length > 0 &&
          ressources.lists.map((ressource: Ressource) => (
            <div className="list-item-card" key={ressource.id} onClick={() => navigate(`/ressource-detail/${ressource.id}`)}>
              <RessourceCard ressource={ressource} />
            </div>
          ))}
      </div>
    </div>
  );
};

export default RessourceList;
