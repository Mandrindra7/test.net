import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { User } from "../models/user";
import { getAll, getUser } from "../service/user";

const initialState = {
  list: [],
};

export const getAllUser = createAsyncThunk("user/getAll", async () => {
  const res = await getAll();
  return res && res.data;
});

export const getUserById = createAsyncThunk("user/getUserById", async (id: string) => {
  const res = await getUser(id);
  return res && res.data;
});

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getAllUser.fulfilled, (state, action) => {
      state.list = action.payload;
      return state;
    });
    builder.addCase(getUserById.fulfilled, (state, action) => {
      return action.payload;
    });
  },
});

export const allUser = (state: any) => state;

export default userSlice.reducer;
