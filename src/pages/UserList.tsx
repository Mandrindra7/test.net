import React, { useEffect, useCallback, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router";

import Card from "../components/Card";
import { User } from "../models/user";
import { getAllUser, allUser } from "../redux/user";
import { AppDispatch } from "../store";
import "../assets/userlist.scss";

const UserList = () => {
  const { users } = useSelector(allUser);
  const dispatch = useDispatch<AppDispatch>();
  const navigate = useNavigate();

  const getAll = useCallback(async () => {
    dispatch(getAllUser());
  }, [dispatch]);

  useEffect(() => {
    getAll();
  }, [getAll]);

  return (
    <div className="list">
      <div className="list-item">
        {users?.list?.length > 0 &&
          users.list.map((user: User) => (
            <div className="list-item-card"key={user.id} onClick={() => navigate(`/user-detail/${user.id}`)}>
              <Card user={user} />
            </div>
          ))}
      </div>
    </div>
  );
};

export default UserList;
