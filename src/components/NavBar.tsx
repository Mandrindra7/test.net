import React, { useState } from "react";
import {  NavLink } from "react-router-dom";

import "../assets/navbar.scss";
const NavBar = () => {
  const [active,setActive] = useState<boolean>()
  return (
    <div className="navbar">
      <div className="nav-bar-list">
        <NavLink  className="link" to="/user-list"  
        children={({ isActive }) => {
            const className = isActive ? "active" : "";
            return (
              <>
               <span>Liste des utilisateurs</span>
                <div className={className}></div>
              </>
            );}}
        />
        <NavLink className="link" to="/ressource-list"

          children={({ isActive }) => {
            const className = isActive ? "active" : "";
            return (
              <>
               <span>Liste des ressources</span>
                <div className={className}></div>
              </>
            );}}
        />
      
     
      </div>
    </div>
  );
};

export default NavBar;
