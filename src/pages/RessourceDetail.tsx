import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { Navigate, useNavigate, useParams } from 'react-router';

import { Ressource } from '../models/ressource';
import { getRessourceById } from '../redux/ressource';
import { AppDispatch } from '../store';
import arrow from '../assets/back.png'
import '../assets/ressourcedetail.scss'

const RessourceDetail = () => {
  const [ressource, setRessource] = useState<Ressource>();
  const dispatch = useDispatch<AppDispatch>();
  const params = useParams();
  const navigate = useNavigate()

  const getRessource = async (id: string) => {
    const res = await dispatch(getRessourceById(id));
    res && setRessource(res.payload.data);
  };

  useEffect(() => {
    const { id } = params;
    id && getRessource(id);
  }, []);

  return (
    <div className='ressource-detail'>
    <div className='back'>
      <img className='img' src={arrow} onClick={() => navigate('/ressource-list')}/>
      <span className='text'> Retour à la liste</span>
    </div>
    <div className='item'>
        <p className='name' style={{background : ressource?.color}}><span>Nom: {ressource?.name}</span></p>
        <p className='value'><span>Valeur:{ressource?.pantone_value}</span></p>
        <p className='year'><span>Année :{ressource?.year}</span></p>
    </div>
    </div>
   
  )
}

export default RessourceDetail