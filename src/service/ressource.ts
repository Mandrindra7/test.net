import axios from "axios";
import { Ressource } from "../models/ressource";

const url ='https://reqres.in/api/unknown'

export const getAll   = async () => {
    const res = await axios.get(`${url}?per_page=${8}`)
    return res && res.data
}

export const getRessource  = async (id: string) => axios.get(`${url}/${id}`)