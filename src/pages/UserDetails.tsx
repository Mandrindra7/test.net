import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router";

import { AppDispatch } from "../store";
import { getUserById } from "../redux/user";
import { User } from "../models/user";
import Card from "../components/Card";
import '../assets/userdetails.scss'

const UserDetails = () => {
  const [user, setUser] = useState<User>();
  const dispatch = useDispatch<AppDispatch>();
  const params = useParams();

  const getUser = async (id: string) => {
    const res = await dispatch(getUserById(id));
    res && setUser(res.payload.data);
  };

  useEffect(() => {
    const { id } = params;
    id && getUser(id);
  }, []);

  return <div className="user-detail">{user && <Card user={user} />}</div>;
};

export default UserDetails;
