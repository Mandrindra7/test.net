import axios from "axios";

const url = 'https://reqres.in/api/users'

export const getAll = async () => {
  const res = await axios.get(`${url}?per_page=${8}`)
  return res && res.data
}

export const getUser = async (id: string) => axios.get(`${url}/${id}`)